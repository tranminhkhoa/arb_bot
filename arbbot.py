import msvcrt
import sys
from datetime import datetime
from math import *
from time import *

from binance.client import Client
from binance.enums import *
from requests import *

import utils
from Kryptonov2 import *

log_file_name = f'[ARBI][{sys.argv[1]}]' + datetime.now().strftime('[%Y-%m-%d][%Hh%Mm%Ss]') + '.txt'
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)-20s %(name)-12s %(message)s',
                    datefmt='%d/%m/%y  %H:%M:%S',
                    handlers=[logging.FileHandler(log_file_name), logging.StreamHandler()])

klogger = logging.getLogger('KRYPTONO')
blogger = logging.getLogger('BINANCE')
flogger = logging.getLogger('BOTFATHER')


class BinanceBot:
    name = 'BINANCE'

    def __init__(self, symbol):
        self.client = Client(config.BINANCE_API_KEYS[symbol]['API_KEY'], config.BINANCE_API_KEYS[symbol]['API_SECRET'])
        self.symbol = symbol.split("_")[0] + symbol.split("_")[1]
        self.get_exchange_info()

        self.initial_first_balance = float(self.client.get_asset_balance(asset=BotFather.first_symbol)['free'])
        self.prev_first_balance = self.initial_first_balance
        self.curr_first_balance = self.initial_first_balance
        self.initial_second_balance = float(self.client.get_asset_balance(asset=BotFather.second_symbol)['free'])
        self.prev_second_balance = self.initial_second_balance
        self.curr_second_balance = self.initial_second_balance

        flogger.info(f'============ {self.name} INITIAL BALANCE ============')
        flogger.info(f'{BotFather.first_symbol}: {self.initial_first_balance}')
        flogger.info(f'{BotFather.second_symbol}: {self.initial_second_balance}')

    def get_exchange_info(self):
        exchange_info = requests.get(config.BINANCE_URL_GET_EXCHANGE_INFO).json()
        for symbol_info in exchange_info['symbols']:
            if symbol_info['symbol'] == self.symbol:
                self.base_asset_precision = symbol_info['baseAssetPrecision']
                self.quote_asset_precision = symbol_info['quotePrecision']
                self.min_qty = float(symbol_info['filters'][1]['minQty'])
                self.min_total_qty = float(symbol_info['filters'][2]['minNotional']) * 1.1
                break

    def get_order_book(self, side):
        try:
            order_book = requests.get(config.BINANCE_URL_GET_ORDER_BOOK + self.symbol + '&limit=5').json()
        except Exception as e:
            blogger.info(f'Cannot get order book. Error message: {e}')
            return False

        if side not in order_book:
            return False

        if not order_book[side]:
            return False

        new_order_book = []
        for order in order_book[side]:
            price = float(format(float(order[0]), '.8f'))
            qty = float(format(float(order[1]), '.8f'))
            new_order_book.append([price, qty])
        # self.buy_price = float(order_book['bids'][0][0])
        # self.buy_qty = float(order_book['bids'][0][1])
        #
        # self.sell_price = float(order_book['asks'][0][0])
        # self.sell_qty = float(order_book['asks'][0][1])

        return new_order_book

    def place_order(self, side, price, qty):
        try:
            order = self.client.create_order(
                symbol=self.symbol,
                side=side,
                type=ORDER_TYPE_LIMIT,
                timeInForce=TIME_IN_FORCE_GTC,
                quantity=qty,
                price=utils.exp_to_str(price))

            if 'orderId' not in order:
                return {'status': 'failure', 'error': 'unknown', 'skip': False}
            return {'status': 'success', 'orderId': order['orderId']}

        except Exception as e:
            return {'status': 'failure', 'error': e, 'skip': False}
        # return {'status': 'success', 'orderId': 1234}

    def cancel_order(self, orderId):
        for _ in range(0, config.MAX_RETRIES):
            try:
                cancel_status = self.client.cancel_order(symbol=self.symbol, orderId=orderId)
                if cancel_status is not None and 'orderId' in cancel_status:
                    return True
            except Exception as e:
                if self.get_order_detail(orderId)['status'] == 'FILLED':
                    return True

            time.sleep(config.WAIT_BETWEEN_CANCEL)

        return False

    def get_order_detail(self, order_id):
        for _ in range(0, config.MAX_RETRIES):
            try:
                order = self.client.get_order(symbol=self.symbol, orderId=order_id)
                return {'status': order['status'], 'orderId': order['orderId'],
                        'executedQty': float(order['executedQty'])}

            except Exception as e:
                blogger.info(f'Cannot get order detail. Error message: {e}')

        return {'status': 'failure'}
        # return {'status': 'FILLED', 'orderId': 1234, 'executedQty': 1}

    def update_balance(self):
        self.prev_first_balance = self.curr_first_balance
        self.prev_second_balance = self.curr_second_balance
        self.curr_first_balance = float(self.client.get_asset_balance(asset=BotFather.first_symbol)['free'])
        self.curr_second_balance = float(self.client.get_asset_balance(asset=BotFather.second_symbol)['free'])


class KryptonoBot:
    name = 'KRYPTONO'

    def __init__(self, symbol):
        self.bot = Kryptono(config.KRYPTONO_API_KEYS[symbol]['API_KEY'], config.KRYPTONO_API_KEYS[symbol]['API_SECRET'])
        self.symbol = symbol.split("_")[0] + '_' + symbol.split("_")[1]
        self.get_exchange_info()
        self.initial_first_balance = float(self.bot.getBalanceAvailable(BotFather.first_symbol))
        self.prev_first_balance = self.initial_first_balance
        self.curr_first_balance = self.initial_first_balance
        self.initial_second_balance = float(self.bot.getBalanceAvailable(BotFather.second_symbol))
        self.prev_second_balance = self.initial_second_balance
        self.curr_second_balance = self.initial_second_balance

        flogger.info(f'============ {self.name} INITIAL BALANCE ============')
        flogger.info(f'{BotFather.first_symbol}: {self.initial_first_balance}')
        flogger.info(f'{BotFather.second_symbol}: {self.initial_second_balance}')

    def get_exchange_info(self):
        exchange_info = requests.get(config.KRYPTONO_URL_API_GET_EXCHANGE_INFO).json()
        for symbol_info in exchange_info['symbols']:
            if symbol_info['symbol'] == self.symbol:
                self.base_asset_precision = symbol_info['amount_limit_decimal']
                self.quote_asset_precision = symbol_info['price_limit_decimal']
                break

        for base_currency in exchange_info['base_currencies']:
            if base_currency['currency_code'] == BotFather.second_symbol:
                self.min_total_qty = float(base_currency['minimum_total_order'])
                break

        for coin_info in exchange_info['coins']:
            if coin_info['currency_code'] == BotFather.first_symbol:
                self.min_qty = float(coin_info['minimum_order_amount'])
                break

    def get_order_book(self, side):
        try:
            order_book = requests.get(config.KRYPTONO_URL_ENGINES_GET_ORDER_BOOK + self.symbol).json()
        except Exception as e:
            klogger.info(f'Cannot get order book. Error message: {e}')
            return False

        if side not in order_book:
            return False

        if not order_book[side]:
            return False

        for order in order_book[side]:
            order[0] = utils.normalize(float(order[0]), self.quote_asset_precision)
            order[1] = float(order[1])

        # self.buy_price = utils.normalize(float(order_book['bids']['price']) / 10 ** 8, self.quote_asset_precision)
        # self.buy_qty = float(order_book['bids']['amount']) / 10 ** 8
        # self.sell_price = utils.normalize(float(order_book['asks']['price']) / 10 ** 8, self.quote_asset_precision)
        # self.sell_qty = float(order_book['asks']['amount']) / 10 ** 8

        return order_book[side]

    def place_order(self, side, price, qty):
        order = self.bot.createOrder(side, self.symbol, price, qty)

        if 'error' in order:
            if 'Minimum amount' not in order['error_description'] and 'Total must be equal or greater than' not in \
                    order['error_description']:
                msg = f"Arbitrage bot {BotFather.first_symbol}/{BotFather.second_symbol} on {self.name} cannot place {side} order. Error message: {order['error_description']}"
                utils.send_telegram_message(msg)
                klogger.info(msg)
                return {'status': 'failure', 'error': order['error_description'], 'skip': False}
            return {'status': 'failure', 'error': order['error_description'], 'skip': True}
        return {'status': 'success', 'orderId': order['order_id']}
        # return {'status': 'success', 'orderId': 1234}

    def get_order_detail(self, order_id):
        order = self.bot.getOrderDetail(order_id)
        klogger.info(f'{self.name}\'s order detail: {order}\n')
        if order is None or 'executedQuantity' not in order:
            return {'status': 'failure', 'executedQty': 0.0}
        return {'status': 'success', 'orderId': order['order_id'], 'executedQty': float(order['executed'])}
        # return {'status': 'success', 'orderId': 1234, 'executedQty': 1}

    def update_balance(self):
        self.prev_first_balance = self.curr_first_balance
        self.prev_second_balance = self.curr_second_balance
        self.curr_first_balance = float(self.bot.getBalanceAvailable(BotFather.first_symbol))
        self.curr_second_balance = float(self.bot.getBalanceAvailable(BotFather.second_symbol))

# A master bot that manages other bots
class BotFather:
    first_symbol, second_symbol = '', ''
    start_time = datetime.now().strftime('%d/%m/%y %H:%M:%S')

    def __init__(self, symbol):
        BotFather.first_symbol, BotFather.second_symbol = symbol.split("_")[0], symbol.split("_")[1]

        self.alice = BinanceBot(symbol)
        self.bob = KryptonoBot(symbol)

        self.base_asset_precision = min(self.alice.base_asset_precision, self.bob.base_asset_precision)
        self.quote_asset_precision = min(self.alice.quote_asset_precision, self.bob.quote_asset_precision)
        self.min_qty = max(self.alice.min_qty, self.bob.min_qty)
        self.min_total_qty = max(self.alice.min_total_qty, self.bob.min_total_qty)

        self.total_initial_first_balance = self.alice.initial_first_balance + self.bob.initial_first_balance
        self.total_initial_second_balance = self.alice.initial_second_balance + self.bob.initial_second_balance

        self.total_profit = 0.0
        self.num_of_txs = 0
        self.finished_eating = False

    def make_decision(self, alice, bob):
        # logging.info('\t\t{}\t\t{}\n'.format(alice.name, bob.name))
        # logging.info('Buy price:\t{0:.8f}\t{1:.8f}'.format(best_bid_price, bob.buy_price))
        # logging.info('Sell price:\t{0:.8f}\t{1:.8f}'.format(alice.sell_price, ask_price))
        # logging.info('Buy quantity:\t{0:.8f}\t{1:.8f}'.format(alice.buy_qty, bob.buy_qty))
        # logging.info('Sell quantity:\t{0:.8f}\t{1:.8f}\n'.format(alice.sell_qty, bob.sell_qty))
        # gap = utils.normalize(best_bid_price - ask_price, self.quote_asset_precision)

        if alice.name == 'KRYPTONO':
            alice_fee_rate = 0
            bob_fee_rate = 0.01
        else:
            alice_fee_rate = 0.01
            bob_fee_rate = 0

        bid_order_book = alice.get_order_book('bids')
        print(f'{alice.name}: {bid_order_book}')
        ask_order_book = bob.get_order_book('asks')
        print(f'{bob.name}: {ask_order_book}')
        best_bid_price = bid_order_book[0][0]
        best_ask_price = ask_order_book[0][0]

        time.sleep(5)

        qty = 0
        ask_price = 0
        gap = 0
        fee = 0

        for ask_order in ask_order_book:
            gap = utils.normalize(best_bid_price - float(ask_order[0]), self.quote_asset_precision)
            fee = utils.normalize(best_bid_price * alice_fee_rate + float(ask_order[0]) * bob_fee_rate, 8)

            if gap <= fee:
                break

            ask_price = float(ask_order[0])
            qty += float(ask_order[1])

        qty = utils.normalize(qty, self.base_asset_precision)

        if qty < self.min_qty or ask_price == 0:
            self.finished_eating = True
            return True

        time.sleep(0.6)

        if alice.get_order_book('bids')[0][0] != best_bid_price:
            time.sleep(config.WAIT_FOR_NEW_ORDER)
            return True
        if bob.get_order_book('asks')[0][0] != best_ask_price:
            time.sleep(config.WAIT_FOR_NEW_ORDER)
            return True

        flogger.info('==================== ARBITRAGE OPPORTUNITY DETECTED ====================')
        flogger.info(
            f'Someone is buying {alice.buy_qty} {BotFather.first_symbol} at (high) price {best_bid_price} {BotFather.first_symbol}/{BotFather.second_symbol} on {alice.name}')
        flogger.info(
            f'Someone is selling {bob.sell_qty} {BotFather.first_symbol} at (low) price {ask_price} {BotFather.first_symbol}/{BotFather.second_symbol} on {bob.name}')
        flogger.info(f'Pricing discrepancy: {gap} {BotFather.first_symbol}/{BotFather.second_symbol}')

        # qty = min(alice.buy_qty, bob.sell_qty)
        #
        # qty = max(qty, self.min_qty)
        # qty = utils.normalize(
        #     max(qty, utils.normalize(self.min_total_qty / best_bid_price, self.base_asset_precision)),
        #     self.base_asset_precision)
        flogger.info(f'Quantity to be placed: {qty} {BotFather.first_symbol}')

        alice.update_balance()
        bob.update_balance()

        if qty * ask_price > bob.curr_second_balance:
            flogger.info(
                f'Cannot place orders since {bob.name} bot does not have enough {BotFather.second_symbol}. Current balance: {bob.curr_second_balance} {BotFather.second_symbol}')
            return True
        if qty > alice.curr_first_balance:
            flogger.info(
                f'Cannot place orders since {alice.name} bot does not have enough {BotFather.first_symbol}. Current balance: {alice.curr_first_balance} {BotFather.first_symbol}')
            return True

        if alice.name == 'BINANCE':
            blogger.info(
                f'Sell {qty} {BotFather.first_symbol} at price {best_bid_price} {BotFather.first_symbol}/{BotFather.second_symbol}')
            sell_order = alice.place_order('sell', best_bid_price, qty)

            if sell_order['status'] == 'failure':
                blogger.info(f"Failed to sell! Error message: {sell_order['error']}. Move on to the next round.")
                blogger.info('========================================================================')
                return True

            blogger.info(
                f"Successfully placed ask order! Order ID: {sell_order['orderId']}. Wait for the order to be filled...")
            time.sleep(config.WAIT_FOR_FILLED)

            blogger.info(f"Cancelling order...")

            cancel_ok = True
            if not alice.cancel_order(sell_order['orderId']):
                blogger.info(f'Cannot cancel order. Error message: {e}')
                cancel_ok = False
            else:
                blogger.info('Successfully cancelled order!')
                cancel_ok = True

            sell_order_detail = alice.get_order_detail(sell_order['orderId'])
            sold_qty = qty

            if sell_order_detail['status'] == 'failure':
                blogger.info(f'Cannot get order detail. Continue placing buy order on {bob.name}.')
                sold_qty = qty
            else:
                if cancel_ok:
                    sold_qty = utils.normalize(sell_order_detail['executedQty'], self.base_asset_precision)
                else:
                    sold_qty = qty

                if sold_qty == 0.0:
                    blogger.info('Sell order was not filled! Move on to the next round.')
                    time.sleep(config.WAIT_AFTER_CANCEL)
                    flogger.info('========================================================================')
                    return True

                blogger.info(f'Sold {sold_qty} {BotFather.first_symbol}')

            klogger.info(f'Buy {sold_qty} {BotFather.first_symbol} at price {ask_price}')
            buy_order = bob.place_order('buy', ask_price, sold_qty)

            if buy_order['status'] == 'failure':
                if buy_order['skip']:
                    klogger.info('Total bid quantity is too small to place! Move on to the next round.')
                    flogger.info('========================================================================')
                    return True

                blogger.info(
                    f"Failed to buy! Error message: {buy_order['error']}. Buy the same quantity at the same price to regain the loss.")

                t = 1
                while True:
                    buy_order = alice.place_order('buy', ask_price, sold_qty)
                    if buy_order['status'] == 'failure':
                        klogger.info(f"Failed to buy! Error message: {buy_order['error']}. Wait for {t} second(s)...")
                        time.sleep(t)
                        t = (t + 1) % config.MAX_WAIT_TIME
                    else:
                        blogger.info(f'Bought {sold_qty} {BotFather.first_symbol}')
                        break
            else:
                klogger.info(f'Bought {sold_qty} {BotFather.first_symbol}')

            profit = sold_qty * (gap - fee)
            self.total_profit += profit

        else:

            blogger.info(
                f'Buy {qty} {BotFather.first_symbol} at price {ask_price} {BotFather.first_symbol}/{BotFather.second_symbol}')
            buy_order = bob.place_order('buy', ask_price, qty)

            if buy_order['status'] == 'failure':
                blogger.info(f"Failed to buy! Error message: {buy_order['error']}. Move on to the next round.")
                flogger.info('========================================================================')
                return True

            blogger.info(
                f"Successfully placed bid order! Order ID: {buy_order['orderId']}. Wait for the order to be filled...")
            time.sleep(config.WAIT_FOR_FILLED)

            blogger.info(f"Cancelling order...")
            cancel_ok = True

            if not bob.cancel_order(buy_order['orderId']):
                blogger.info(f'Cannot cancel order. Error message: {e}')
                cancel_ok = False
            else:
                blogger.info('Successfully cancelled order!')
                cancel_ok = True

            buy_order_detail = bob.get_order_detail(buy_order['orderId'])
            bought_qty = qty

            if buy_order_detail['status'] == 'failure':
                blogger.info(f'Cannot get order detail. Continue placing sell order on {alice.name}.')
                bought_qty = qty
            else:
                if cancel_ok:
                    bought_qty = utils.normalize(buy_order_detail['executedQty'], self.base_asset_precision)
                else:
                    bought_qty = qty

                if bought_qty == 0.0:
                    blogger.info('Buy order was not filled! Move on to the next round.')
                    time.sleep(config.WAIT_AFTER_CANCEL)
                    flogger.info('========================================================================')
                    return True

                blogger.info(f'Bought {bought_qty} {BotFather.first_symbol}.')

            klogger.info(f'Sell {bought_qty} {BotFather.first_symbol} at price {best_bid_price}')
            sell_order = alice.place_order('sell', best_bid_price, bought_qty)

            if sell_order['status'] == 'failure':
                if sell_order['skip']:
                    klogger.info('Total ask quantity is too small to place! Move on to the next round.')
                    flogger.info('========================================================================')
                    return True

                blogger.info(
                    f"Failed to sell! Error message: {sell_order['error']}. Sell the same quantity at the same price to regain the loss.")

                t = 1
                while True:
                    sell_order = bob.place_order('sell', best_bid_price, bought_qty)
                    if sell_order['status'] == 'failure':
                        blogger.info(f"Failed to sell! Error message: {sell_order['error']}. Wait for {t} second(s)...")
                        time.sleep(t)
                        t = (t + 1) % config.MAX_WAIT_TIME
                    else:
                        blogger.info(f'Sold {bought_qty} {BotFather.first_symbol}')
                        break
            else:
                klogger.info(f'Sold {bought_qty} {BotFather.first_symbol}')

            profit = bought_qty * (gap - fee)
            self.total_profit += profit

        self.num_of_txs += 1

        total_prev_first_balance = alice.prev_first_balance + bob.prev_first_balance
        total_prev_second_balance = alice.prev_second_balance + bob.prev_second_balance
        total_curr_first_balance = alice.curr_first_balance + bob.curr_first_balance
        total_curr_second_balance = alice.curr_second_balance + bob.curr_second_balance

        flogger.info(f'Profit of this round: {profit} {BotFather.second_symbol}')
        flogger.info(f"Total profit: {self.total_profit} {BotFather.second_symbol}")
        flogger.info(f"Number of transactions: {self.num_of_txs}")

        alice.update_balance()
        bob.update_balance()

        flogger.info('Balance statistics')

        headers = ['', 'Asset', 'Before', 'After', 'Diff.']

        rows = []
        rows.append([f'{alice.name}', BotFather.first_symbol, alice.prev_first_balance, alice.curr_first_balance,
                     alice.curr_first_balance - alice.prev_first_balance])
        rows.append(['', BotFather.second_symbol, alice.prev_second_balance, alice.curr_second_balance,
                     alice.curr_second_balance - alice.prev_second_balance])
        rows.append([f'{bob.name}', BotFather.first_symbol, bob.prev_first_balance, bob.curr_first_balance,
                     bob.curr_first_balance - bob.prev_first_balance])
        rows.append(['', BotFather.second_symbol, bob.prev_second_balance, bob.curr_second_balance,
                     bob.curr_second_balance - bob.prev_second_balance])
        rows.append(['BOTH', BotFather.first_symbol, total_prev_first_balance, total_curr_first_balance,
                     total_curr_first_balance - total_prev_first_balance])
        rows.append(['', BotFather.second_symbol, total_prev_second_balance, total_curr_second_balance,
                     total_curr_second_balance - total_prev_second_balance])

        flogger.info('_' * 80)
        flogger.info('{:>8} {:>8} {:>20} {:>20} {:>20}'.format(*headers))
        flogger.info('_' * 80)
        for row in rows:
            flogger.info('{:>8} {:>8} {:20.8f} {:20.8f} {:20.8f}'.format(*row))
        flogger.info('_' * 80)

        flogger.info('========================================================================')

        self.finished_eating = False
        return True

    def run(self):
        t = 1

        while True:
            try:
                if msvcrt.kbhit() and ord(msvcrt.getch()) == 27:
                    break

                # if not self.alice.get_order() or not self.bob.get_order():
                #     time.sleep(t)
                #     t = (t + 1) % config.MAX_WAIT_TIME
                #     continue

                # t = 1

                # if self.alice.buy_qty < self.min_qty or self.bob.sell_qty < self.min_qty:
                #     time.sleep(config.WAIT_FOR_NEW_ORDER)

                if not self.make_decision(self.alice, self.bob) or not self.make_decision(self.bob, self.alice):
                    utils.send_telegram_message(
                        f'Bot {BotFather.first_symbol}/{BotFather.second_symbol} stopped because of an unexpected error!')
                    break

                if self.finished_eating:
                    time.sleep(config.WAIT_FOR_NEW_ORDER)

            except Exception as e:
                msg = f'An unexpected error occurred. Error message: {e}. Wait for {t} second(s)...'
                flogger.info(msg)
                utils.send_telegram_message(msg)
                sleep(t)
                t = (t + 1) % config.MAX_WAIT_TIME


if __name__ == '__main__':
    bot = BotFather(symbol=sys.argv[1]).run()