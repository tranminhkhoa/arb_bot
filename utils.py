import requests
import math

BOT_TOKEN = '604591409:AAH3AV-pCqnmWPwQkLb_a1FoizIutkhTjoE'
CHAT_ID = '-257652150'

def send_telegram_message(msg):
    requests.get(f"https://api.telegram.org/bot{BOT_TOKEN}/sendMessage?text={msg}&chat_id={CHAT_ID}")

def exp_to_str(value):
    """
    Convert exponential format (e.g., 3.4411e-05 to string
    e.g., 0.000034411
    """
    value_str = str(value)

    if 'e' not in value_str and 'E' not in value_str:
        return value_str

    real = value_str[: value_str.index('e')]
    exp = value_str[value_str.index('e') + 1:]

    real_str = real.replace('.', '')
    for _ in range(0, abs(int(exp)) - 1):
        real_str = '0' + real_str
    real_str = '0.' + real_str

    return real_str


# normalize quantity number before sending orders
def normalize(num, precision):
    return math.trunc(num * 10**precision) / 10**precision